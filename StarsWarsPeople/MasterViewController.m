//
//  MasterViewController.m
//  StarsWarsPeople
//
//  Created by James Cash on 22-05-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"

@interface MasterViewController ()

@property NSArray* starWarsPeople;
@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;

    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];

    NSURL *endpoint = [NSURL URLWithString:@"https://swapi.co/api/people/"];
    NSURLRequest *request = [NSURLRequest requestWithURL:endpoint];
    NSLog(@"Making a %@ request to %@", request.HTTPMethod, request.URL);

    NSURLSessionTask *task =
    [[NSURLSession sharedSession]
     // could use dataTaskWithURL: instead
     dataTaskWithRequest:request
     completionHandler:^(NSData*  data, NSURLResponse*  response, NSError*  error) {
         NSLog(@"Got response %@ (error? %@)", response, error);
         if (error != nil) {
             NSLog(@"Something went wrong getting request: %@", error.localizedDescription);
             abort();
         }

         NSError *jsonErr = nil;
         // Figured out this was a dictionary (not, e.g. an array) by looking at docs
         NSDictionary* parsedJSON = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:&jsonErr];
         if (jsonErr) {
             NSLog(@"Something went wrong parsing json: %@", jsonErr.localizedDescription);
             abort();
         }
         NSLog(@"parsed %@", parsedJSON);
         NSMutableArray<NSString*> *people = [[NSMutableArray alloc] init];
         // figured out the results key by logging at API docs
         for (NSDictionary *person in parsedJSON[@"results"]) {
             [people addObject:person[@"name"]];
         }
         self.starWarsPeople = people;
         [[NSOperationQueue mainQueue] addOperationWithBlock:^{
             [self.tableView reloadData];
         }];
     }];

    [task resume];
}


- (void)viewWillAppear:(BOOL)animated {
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Segues

/*
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDate *object = self.objects[indexPath.row];
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        [controller setDetailItem:object];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}
*/

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.starWarsPeople.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    NSString *object = self.starWarsPeople[indexPath.row];
    cell.textLabel.text = object;
    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


/*
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}
*/

@end
